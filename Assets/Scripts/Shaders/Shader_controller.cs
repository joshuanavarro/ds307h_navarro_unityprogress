﻿using Bolt;
using Ludiq;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class Shader_controller : MonoBehaviour
{
    Text Objectname;

    private void Start()
    {
        Objectname = GetComponentInChildren<Text>();
    }
    private void OnMouseEnter()
    {
        //Find Outline Shader & increases "_Outline" on Enter
        GetComponentInChildren<Renderer>().material.SetFloat("_Outline", .05f);
        Objectname.text = gameObject.name;

    }
    private void OnMouseExit()
    {
        GetComponent<Renderer>().material.SetFloat("_Outline", 0f);
        Objectname.text = null;


    }
}

