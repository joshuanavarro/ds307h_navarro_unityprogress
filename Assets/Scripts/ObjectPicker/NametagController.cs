﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//summry
// This code should be added to all 'nametags'
//summery/
public class NametagController : MonoBehaviour
{
    GameObject MainCamera;
    Text Objectname;

    private float Speed = 10.0f;
    private bool isRotating = false;

    GameObject ObjectB;


    private void Start()
    {
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        Objectname = GetComponentInChildren<Text>();
    

    }

    void Update()
    {
        Objectname.transform.position = Objectname.transform.parent.position;
        //Starts rotation if Objectname is not null
        if (Objectname != null)
            isRotating = !isRotating; // < Starts roation

        if (isRotating)
            SetRotate(Objectname.gameObject, MainCamera);

        if (Objectname.transform.rotation.eulerAngles.y == MainCamera.transform.rotation.eulerAngles.y)
            isRotating = !isRotating;
    }

    void SetRotate(GameObject Objectname, GameObject camera)
    {
        transform.rotation = UnityEngine.Quaternion.Lerp(Objectname.transform.rotation, camera.transform.rotation, Speed * Time.deltaTime);
    }


}
