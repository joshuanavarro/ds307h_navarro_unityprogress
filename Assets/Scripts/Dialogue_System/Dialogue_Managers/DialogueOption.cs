﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogueTree
{
    public class DialogueOption
    {
        public string Text;
        public int DestinationNodeID;

        //parameterless constructor for serialization
        public DialogueOption() { }

        public DialogueOption(string text, int dest)
        {
            this.Text = text;
            this.DestinationNodeID = dest;

        }
    }
}
