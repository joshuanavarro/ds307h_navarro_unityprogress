﻿using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;
public class VoiceMovement : MonoBehaviour
{
    // add in keywords to Dictionary
    private KeywordRecognizer keywordRecognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    void Start()
    {
        // (word that is regognized, Action Variaible)
        actions.Add("adelante", Forward);
        actions.Add("arriba", Up);
        actions.Add("abajo", Down);
        actions.Add("atras", Back);
        // when keyword is heard indicate RecognizedSpeech event
        keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        keywordRecognizer.Start();

    }
    private void RecognizedSpeech(PhraseRecognizedEventArgs speech)
    {
        // Debug what phrase was said
        Debug.Log(speech.text);
        actions[speech.text].Invoke();
    }
    // Actions
    private void Forward()
    {
        transform.Translate(1, 0, 0);
    }

    private void Back()
    {
        transform.Translate(-1, 0, 0);
    }
    private void Up()
    {
        transform.Translate(0, 1, 0);
    }

    private void Down()
    {
        transform.Translate(0, -1, 0);
    }

}
